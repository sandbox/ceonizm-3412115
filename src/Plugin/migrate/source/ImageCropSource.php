<?php

namespace Drupal\migrate_convert_imagefield_crop\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate\Row;

/**
 *
 * @MigrateSource(
 *    id = "d7_image_cropsource",
 *    source_module = "migrate_convert_imagefield_crop"
 *   )
 */
class ImageCropSource extends ImageFieldCropSource
{

    /**
     * @inheritDoc
     */
    public function fields() {
        // TODO: Implement fields() method.
    }

    /**
     * @inheritDoc
     */
    public function getIds() {
        // TODO: Implement getIds() method.
    }



    public function prepareRow(Row $row) {
        /** @var Row $row */
        $row = parent::prepareRow($row);

        $fieldName = $row->getSourceProperty('field_name');
        /** @var SelectInterface $select */
        $select = $this->select('field_data_'.$fieldName, 'f')
        ->fields('f')
        ->addField('fu', 'fid', 'original_fid');
        $joinAlias = $select->leftJoin('file_usage', 'fu', 'f.'.$fieldName.'_fid = fu.id');
        $select->condition('fu.module', 'imagefield_crop');
        $replacementMap = [];
        $res = $select->execute();

        if($res && $res->rowCount()) {
            while( $fieldRow = $res->fetchObject()) {
                $replacementMap[ $fieldRow->{"${fieldName}_fid"} ] = $fieldRow->original_fid;
            }
        }
        $row->setSourceProperty('replacementFids', $replacementMap);
        return $row;
    }
}
