<?php

namespace Drupal\migrate_convert_imagefield_crop\Plugin\migrate\source;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_convert_imagefield_crop\Plugin\migrate\process\ExtractCropDefinition;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 *
 * @MigrateSource(
 *    id = "d7_image_crops_extract",
 *    source_module = "migrate_convert_imagefield_crop"
 *   )
 */
class ExtractCropSource extends DrupalSqlBase
{

    public function fields() {
        return [
            'cropFileId' => 'the cropped file id',
            'field_name' => 'the field name the crop belongs to',
            'fid' => 'the original file id',
            'nid' => 'the node id the file belongs to',
            'uri' => 'the uri of the file',
            'entity_type' => 'the entity type'
        ];
    }

    public function getIds() {
        return [
            'fid' => [
                'type' => 'integer',
                'alias' => 'fid'
            ]
        ];
    }

    public function query() {
        // TODO: Implement query() method.

        $q = $this->select('file_usage', 'fu1');
        $q->addJoin('RIGHT OUTER', 'file_usage', 'fu2', 'fu1.id = fu2.fid');
        $q->leftJoin('file_managed', 'fm', 'fu1.fid = fm.fid');
        $q->addField('fu1', 'fid', 'fid');
        $q->addField('fm', 'uri');
        $q->addField('fu1', 'id', 'cropFileId');
        $q->addField('fu2', 'id', 'nid');
        $q->addField('fu2', 'type', 'entity_type');
        $q->orderBy('nid')
            ->condition('fu1.module', 'imagefield_crop')
            ->condition('fu2.type', 'node');


        return $q;
    }

    public function prepareRow(Row $row) {

        /** @var \Drupal\migrate\Plugin\MigratePluginManager $pluginProcessManager */
        $pluginProcessManager = \Drupal::service('plugin.manager.migrate.process');
        $cropData = $this->variableGet('imagefield_crop_info', []);

        $cropFileId = $row->getSourceProperty('cropFileId');
        $fid = $row->getSourceProperty('fid');
        $nid = $row->getSourceProperty('nid');

        if (array_key_exists($cropFileId, $cropData) && !empty($cropData[$cropFileId])) {
            $targetCrop = $cropData[$cropFileId];
            $x = (int) $targetCrop['x'];
            $y = (int) $targetCrop['y'];
            $width = (int) $targetCrop['width'];
            $height = (int) $targetCrop['height'];
            if( $x < 0) {
                $width -= (0 - $x);
                $x = 0;
            }
            if( $y < 0) {
                $height -= (0 - $y);
                $y = 0;
            }
            $row->setSourceProperty('x', $x + round($width/2));
            $row->setSourceProperty('y', $y + round($height/2));
            $row->setSourceProperty('width', $width);
            $row->setSourceProperty('height', $height);
        }

        // lookup into image field names
        $nq = $this->select('node', 'n');
        $nq->addField('n', 'type');
        $nq->groupBy('type');
        $nq->condition('nid', $nid);

        $q = $this->select('field_config_instance', 'fci');
//        $q->leftJoin($nq, 'n', 'n.type = fci.bundle');
        $q->addField('fci', 'field_name');
        $q->addField('fci', 'bundle');
        $q->addField('fci', 'data');

        $q->condition('fci.bundle', $nq)
          ->condition('fci.entity_type', 'node')
          ->condition('fci.data', '%' . $this->getDatabase()->escapeLike('imagefield_crop') . '%', 'LIKE');

        $res = $q->execute();
        if ($res) {
            foreach ($res as $line) {
                $fieldInstanceSettings = unserialize( $line['data']);
                if( $fieldInstanceSettings && array_key_exists('widget', $fieldInstanceSettings)) {
                    try {
                        $row->setSourceProperty('bundle', $line['bundle']);
                        $row->setSourceProperty('widget', $fieldInstanceSettings['widget']);
                        /** @var ExtractCropDefinition $process */
                        $process = $pluginProcessManager->createInstance('process_extract_crop_definition', [], $this->migration);

                        $cropId = "";
                        $cropId = $process->transform($cropId, new MigrateExecutable($this->migration), $row, 'id');
                        if( !empty($cropId)) {
                            $row->setSourceProperty('type', $cropId);
                        }
                    } catch (PluginException $e) {
                    }
                }
                $tableName = "field_data_{$line['field_name']}";
                $q = $this->select($tableName, 'f')
                    ->condition('entity_type', 'node')
                    ->condition('entity_id', $nid)
                    ->condition("{$line['field_name']}_fid", $cropFileId);
                $q->addField('f', 'entity_id');
                $result = $q->execute();
                if ($result ) {
                    $fetched = $result->fetch();
                    if(!empty($fetched)) {
                        $row->setSourceProperty('field_name', $line['field_name']);
                        break;
                    }

                }

            }
        }
        return parent::prepareRow($row); // TODO: Change the autogenerated stub
    }
}
