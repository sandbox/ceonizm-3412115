<?php

namespace Drupal\migrate_convert_imagefield_crop\Plugin\migrate\source;
use Drupal\field\Plugin\migrate\source\d7\FieldInstance;
use Drupal\migrate\Annotation\MigrateSource;

/**
 *
 * @MigrateSource(
 *   id = "d7_image_crops",
 *   source_module = "migrate_convert_imagefield_crop"
 *  )
 */
class ImageFieldCropSource extends FieldInstance
{



    public function query() {
        // TODO: Implement query() method.
        /** @var \SelectQueryInterface $select */
        $select = parent::query()
            ->condition('fc.type', 'image')
            ->condition('fci.data', '%'.$this->getDatabase()->escapeLike('imagefield_crop').'%', 'LIKE');
        return $select;
    }

}
