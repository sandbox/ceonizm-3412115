<?php

namespace Drupal\migrate_convert_imagefield_crop\Plugin\migrate\process;

use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_convert_imagefield_crop\MigrateWidgetCropManager;


/**
 *
 *
 * @MigrateProcessPlugin(
 *    id = "process_extract_crop_definition"
 *  )
 */
class ExtractCropDefinition extends ProcessPluginBase
{

    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

        $widget = $row->getSourceProperty('widget');
        if( $widget ) {
            return MigrateWidgetCropManager::transformFromWidget($widget, $value, $destination_property);
        }
        return $value;
    }


    protected static function __gcd( $a, $b) {
        return $b === 0 ? $a : self::__gcd($b, $a % $b);
    }
}
