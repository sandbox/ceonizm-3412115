<?php

namespace Drupal\migrate_convert_imagefield_crop;

use Drupal\migrate\Plugin\MigratePluginManager;
use Drupal\migrate\Plugin\MigrateProcessInterface;

class MigrateWidgetCropManager
{
    private MigratePluginManager $_migrateProcessPluginManager;

    /**
     * @var \Drupal\Core\Plugin\ContainerFactoryPluginInterface|mixed|object
     */
    private MigrateProcessInterface $_processPluginInstance;

    public function __construct(MigratePluginManager $migrateProcessPluginManager) {
        $this->_migrateProcessPluginManager = $migrateProcessPluginManager;
    }

    /**
     * process the widget passed by reference in argument
     * @param $widget
     * @return bool
     */
    public function processWidget(&$widget): bool {
        if ($widget['type'] === 'imagefield_crop_widget') {
            $cropId = self::transformFromWidget($widget, '', 'id');
            if (!empty($cropId)) {
                $widget = [
                    'type' => 'image_widget_crop',
                    'settings' => [
                        'progress_indicator' => 'throbber',
                        'preview_image_style' => 'thumbnail',
                        'crop_preview_image_style' => 'crop_thumbnail',
                        'crop_list' => [
                            $cropId
                        ],
                    ]
                ];
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * process the display passed by reference in argument
     * @param $widget
     * @param $display
     * @return bool
     */
    public function processDisplay( &$widget, &$display): bool {
        $imageStyleMachineName = self::transformFromWidget($widget, "", 'name');
        if (!empty($imageStyleMachineName)) {
            $display['default']['settings']['image_style'] = $imageStyleMachineName;
            return TRUE;
        }
        return FALSE;
    }

    public function processData(&$data): bool {
        $_data = unserialize($data);
        if ($_data && is_array($_data)) {
            if (array_key_exists('widget', $_data) && array_key_exists('type', $_data['widget'])) {
                if ($_data['widget']['type'] === 'imagefield_crop_widget') {

                    $w = $_data['widget'];
                    $modified1 = $this->processWidget($_data['widget']);
                    $modified2 = FALSE;
                    if( array_key_exists('display', $_data) ) {
                        $modified2 = $this->processDisplay($w, $_data['display']);
                    }
                    if ($modified1 || $modified2) {
                        $data = serialize($_data);
                        return TRUE;
                    }
                }

            }
        }
        return FALSE;
    }

    public static function transformFromWidget($widget, $value, $destination_property) {
        switch( $destination_property ) {
            case 'id':
            case 'name':
                $value = 'crop_'.trim($widget['settings']['resolution']).'_'.trim($widget['settings']['croparea']);
            break;
            case 'label':
                $value = trim($widget['settings']['resolution']).' - '.trim($widget['settings']['croparea']);
            break;
            case 'aspect_ratio':
                [ $width, $height ] = explode('x', $widget['settings']['resolution']);
                $gcd = self::__gcd((float)$width, (float) $height );
                $value = intval($width / $gcd).':'.intval($height / $gcd);
            break;
            case 'soft_limit_width':
            case 'hard_limit_width':
                [ $width, $height ] = explode('x', $widget['settings']['resolution']);
                $value = intval( $width / 2 );
            break;
            case 'soft_limit_height':
            case 'hard_limit_height':
                [ $width, $height ] = explode('x', $widget['settings']['resolution']);
                $value = intval( ( $width / 2 ) * $width / $height);
            break;
            case 'effects':
                $value = [[
                    'id' => 'crop_crop',
                    'weight' => 1,
                    'data' => [
                        'crop_type' => self::transformFromWidget($widget, $value, 'id')
                    ]
                ]];
                if( isset($widget['settings']['resolution'])) {
                    [ $width, $height ] = explode('x', $widget['settings']['resolution']);
                    $value[] = [
                        'id' => 'image_scale',
                        'weight' => 2,
                        'data' => [
                            'width' => $width,
                            'height' => $height
                        ]
                    ];
                }
            break;
        }
        return $value;
    }

    protected static function __gcd( $a, $b) {
        return $b === 0 ? $a : self::__gcd($b, $a % $b);
    }
}
