<?php

namespace Drupal\migrate_convert_imagefield_crop\EventSubscribers;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\Plugin\MigratePluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * For some reasons specific field form settings (widget) and field view settings
 * are not assigned to the destination during processing phase.
 * This class aims to redo the assignation once the process is done.
 */
class MigratePreSaveRow implements EventSubscriberInterface
{

    private MigratePluginManager $_migrateProcessPluginManager;

    public function __construct(MigratePluginManager $migrateProcessPluginManager) {
        $this->_migrateProcessPluginManager = $migrateProcessPluginManager;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array {
        // TODO: Implement getSubscribedEvents() method.
        return [
            MigrateEvents::PRE_ROW_SAVE => ['preRowSave']
        ];
    }


    public function preRowSave(MigratePreRowSaveEvent $event): void {
        $migration = $event->getMigration();

        switch ($migration->getSourcePlugin()->getPluginId()) {
            case 'd7_field_instance_widget_settings':
            case 'd7_field_instance_per_form_display':
                $row = $event->getRow();
                if ($row->hasSourceProperty('widget')) {
                    $widget = $row->getSourceProperty('widget');
                    if ($widget['type'] === 'image_widget_crop') {
                            $options = $row->getDestinationProperty('options');
                            $options['settings'] = $widget['settings'];
                            $row->setDestinationProperty('options', $options);
                    }
                }
            break;
            case 'd7_field_instance_per_view_mode':
                $row = $event->getRow();
                if ($row->hasSourceProperty('widget')) {
                    $widget = $row->getSourceProperty('widget');
                    if ($widget['type'] === 'image_widget_crop' && $row->hasSourceProperty('display')) {
                        $display = $row->getSourceProperty('display');
                        $options = $row->getDestinationProperty('options');
                        $options['settings'] = $display['default']['settings'];
                        $row->setDestinationProperty('options', $options);
                    }
                }
            break;
        }
    }
}
