<?php

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrateSourcePluginManager;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_convert_magefield_crop\MigrateWidgetCropManager;

/**
 * Implements hook_migrate_prepare_row().
 */
function migrate_convert_imagefield_crop_migrate_prepare_row(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
    $migrationId = $migration->id();

    /** @var MigrateWidgetCropManager $manager */
    $manager = Drupal::service('migrate_crop_manager');
    switch ($source->getPluginId()) {
        case 'd7_field':
        case 'd7_field_instance':
        case 'd7_field_instance_widget_settings':
        case 'd7_field_instance_per_form_display':
        case 'd7_field_instance_per_view_mode':
            $modified = FALSE;
            $fieldType = $row->getSourceProperty('type');
            if ($fieldType === 'image') {
                $widget = NULL;
                if ($row->hasSourceProperty('display') && $row->hasSourceProperty('widget')) {
                    $widget = $row->getSourceProperty('widget');
                    if ($widget['type'] === 'imagefield_crop_widget') {
                        $display = $row->getSourceProperty('display');
                        if ($manager->processDisplay($widget, $display)) {
                            $row->setSourceProperty('display', $display);
                        }
                    }
                }

                if ($row->hasSourceProperty('widget')) {
                    $widget = $row->getSourceProperty('widget');
                    if ($widget['type'] === 'imagefield_crop_widget') {
                        if ($manager->processWidget($widget)) {
                            $row->setSourceProperty('widget', $widget);
                        }
                    }
                }

                if ($row->hasSourceProperty('data')) {
                    $data = $row->getSourceProperty('data');
                    if ($manager->processData($data)) {
                        $row->setSourceProperty('data', $data);
                    }
                }

                $instances = $row->getSourceProperty('instances');
                if ($instances !== NULL) {
                    foreach ($instances as $idx => $instance) {
                        if ($manager->processData($instance['data'])) {
                            $instances[$idx] = $instance;
                            $modified = TRUE;
                        }
                    }
                    if ($modified) {
                        try {
                            $row->setSourceProperty('instances', $instances);
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                    }
                }
            }
        break;
    }
    if (strpos($migrationId, 'd7_node_complete') !== false) {
        $_source = $row->getSource();
        /** @var MigrateSourcePluginManager $pluginSourceManager */
        $pluginSourceManager = Drupal::service('plugin.manager.migrate.source');
        $currentBundle = $row->getSourceProperty('type');

        try {
            /** @var Drupal\migrate\Plugin\MigratePluginManager $migrationManager */
            $migrationManager = Drupal::service('plugin.manager.migration');
            // build a list of impacted field names

            /** @var Drupal\migrate\Plugin\MigrationInterface $instance */
            $instance = $migrationManager->createInstance('d7_imagefield_crops', [], NULL);
            $instance->getIdMap()->prepareUpdate();
            /** @var Drupal\migrate\Plugin\MigrateSourceInterface $src */
            $src = $pluginSourceManager->createInstance('d7_image_crops_extract', [], $instance);
            $src->rewind();
            $found = FALSE;
            $nid = $row->getSourceProperty('nid');
            while (!$found && $src->valid()) {
                /** @var Drupal\migrate\Row $r */
                $r = $src->current();
                if ($r->getSourceProperty('nid') == $nid) {
                    $modified = FALSE;
                    $fName = $r->getSourceProperty('field_name');
                    if ($fName !== NULL && $row->hasSourceProperty($fName)) {
                        $field = $row->getSourceProperty($fName);
                        if ($field) {

                            foreach ($field as $idx => $spec) {
                                if (array_key_exists('fid', $spec) && $spec['fid'] == $r->getSourceProperty('cropFileId')) {
                                    $field[$idx]['fid'] = $r->getSourceProperty('fid');
                                    $modified = TRUE;
                                }
                            }
                            if ($modified) {
                                try {
                                    $row->setSourceProperty($fName, $field);
//                                    $found = TRUE;
                                } catch (Exception $e) {
                                    echo $e->getMessage();
                                }
                            }
                        }
                    }
                }
                $src->next();
            }
        } catch (PluginException $e) {
        }
    }
}

/**
 * Implements hook_migration_plugins_alter().
 */
function migrate_convert_imagefield_crop_migration_plugins_alter(array &$migrations) {

    $migrationsKeys = array_keys($migrations);
    foreach (['d7_field_instance', 'd7_field_instance_widget_settings'] as $name) {
        $filtered = array_filter($migrationsKeys, function ($ele) use ($name) {
            return preg_match('/' . $name . '$/', $ele);
        });
        if (is_array($filtered) && count($filtered)) {
            foreach ($filtered as $key) {
                if (array_key_exists($key, $migrations)) {
                    $migrations[$key]['migration_dependencies']['required'][] = 'd7_imagefield_crops';
                }
            }
        }
    }
}
